'''
Created on Nov 14, 2015
pillow impl

@author: rubenlalinde
'''

def decode_loc(posX, posY):
    return posX + posY * 100

from PIL import Image
import morse_code_module

im = Image.open("test.png")
prev_code = 0
msg = ''
for j in range(0, im.height - 1):
    for i in range(0, im.width - 1):
        if im.getpixel((i, j)) == 1:
            new_code = decode_loc(i , j)
            msg+= chr(new_code - prev_code)
            prev_code = new_code

print(morse_code_module.decodeMorse(msg,0))
