import zlib

chunk_type_length = 4
crc_length = 4
reference_png_signature = list([137, 80, 78, 71, 13, 10, 26, 10])

# -----------------------------------------------------------------------------

def convertBytesToInt(bytesToConvert):
    return int.from_bytes(bytesToConvert, byteorder='big', signed=False)

# -----------------------------------------------------------------------------

def processIHDRChunk(ihdrChunk):
    print('IHDR Chunk ', ihdrChunk)
    print('Width ', convertBytesToInt(ihdrChunk[0:4]))
    print('Height ', convertBytesToInt(ihdrChunk[4:8]))
    print('Bit Depth ', convertBytesToInt(ihdrChunk[8:9]))
    print('Color Type ', convertBytesToInt(ihdrChunk[9:10]))
    print('Compression Method ', convertBytesToInt(ihdrChunk[10:11]))
    print('Filter Method ', convertBytesToInt(ihdrChunk[11:12]))
    print('Interlace Method ', convertBytesToInt(ihdrChunk[11:12]))
    print('CRC', ihdrChunk[12:16])
    print('==================================================================')
                                    
# -----------------------------------------------------------------------------

def processPLTEChunk(plteChunk, numPlteEntries):
    print('PLTE Chunk ', plteChunk)
    print('Number of PLTE Entries ', int(numPlteEntries / 3))
    
    for i in range(0, numPlteEntries, 3):
        print('-Palette-', int(i / 3))
        print('Red ', convertBytesToInt(plteChunk[i:i + 1]))
        print('Green ', convertBytesToInt(plteChunk[i + 1:i + 2]))
        print('Blue ', convertBytesToInt(plteChunk[i + 2:i + 3]))

    print('CRC', plteChunk[numPlteEntries:numPlteEntries+4])
    print('==================================================================')    

# -----------------------------------------------------------------------------

def processIDATChunk(idatChunk):
    decompressed = zlib.decompress(idatChunk[0:len(idatChunk)-4])
    
    for byteIndex, byte in enumerate(decompressed):
        bits = bin(byte)[2:].zfill(8)
        for bitIndex, bit in enumerate(bits):
            if bit == '1':
                print(byteIndex)
                print(8*byteIndex+bitIndex-8)
    
    print('\r')
    print('IDAT Chunk ', idatChunk)
    print('IDAT Decompression', decompressed)
    print('CRC', idatChunk[len(idatChunk)-4:len(idatChunk)])
    print('==================================================================')  

# -----------------------------------------------------------------------------

def processsRGBChunk(srgbChunk):
    print('sRGB Chunk ', srgbChunk)
    print('sRGB Chunk ', srgbChunk[0])
    print('CRC', srgbChunk[1:5])
    print('==================================================================')  
    
# -----------------------------------------------------------------------------

def processgAMAChunk(gamaChunk):
    print('gAMA Chunk ', gamaChunk)
    print('gAMA Chunk ', gamaChunk[0:4])
    print('CRC', gamaChunk[4:8])
    print('==================================================================')  
    
# -----------------------------------------------------------------------------

def processpHYsChunk(physChunk):
    print('pHYs Chunk ', physChunk)
    print('pHYs Chunk ', physChunk[0:9])
    print('CRC', physChunk[9:13])
    print('==================================================================')  
    
# -----------------------------------------------------------------------------

def processIENDChunk(iendChunk):
    print('IEND Chunk ', iendChunk)
    print('CRC', iendChunk[0:4])
    print('==================================================================')  
    
# -----------------------------------------------------------------------------

def analyzePng(image_file):
    while(True):
        chunk_length = int.from_bytes(image_file.read(4), byteorder='big', signed=False)
        print("Chunk Length ", chunk_length)
        chunk = image_file.read(chunk_type_length + chunk_length + crc_length)
        print("Chunk ", chunk)
        chunk_type = chunk[0:4].decode()
        #print("Chunk Type ", chunk_type)
        
        if chunk_type == 'IHDR':
            processIHDRChunk(chunk[4:])
        elif chunk_type == 'PLTE':
            processPLTEChunk(chunk[4:], chunk_length)
        elif chunk_type == 'IDAT':
            processIDATChunk(chunk[4:])
        elif chunk_type == 'sRGB':
            processsRGBChunk(chunk[4:])
        elif chunk_type == 'gAMA':
            processgAMAChunk(chunk[4:])
        elif chunk_type == 'pHYs':
            processpHYsChunk(chunk[4:])
        elif chunk_type == 'IEND':
            processIENDChunk(chunk[4:])
        else:
            print('Analysis Complete')
            break

# -----------------------------------------------------------------------------

image_file = open('test.png', 'r+b')
image_signature = list(image_file.read(8))

if image_signature == reference_png_signature:
    #print('Analyzing PNG File')
    analyzePng(image_file)
else:
    print('File Format Not Supported')
